import socket
import traceback

from datapacket import DataPacket

theboys = []

class Server:
    def __init__(self, port):
        self.port = port

    def acceptance(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(('', self.port))
        # with no backlogged connections
        self.sock.listen(0)
        print("listening on %i" % self.port)
        try:
            while True:
                (conn, addr) = self.sock.accept()
                data = conn.recv(20)
                ## Accept a backend brother
                if data == b"pls accept":
                    packet = "Middle: Welcome to the fold, brother."
                    conn.send(packet.encode('UTF-8'))
                    theboys.append(conn)
                    print("adding another manling")
                ## Give the frontend something
                elif data == b"gimme":
                    backend = None
                    # get a live socket
                    for i,b in enumerate(theboys):
                        try:
                            backend = b
                            backend.send("gimme".encode())
                            print("requesting from backend")
                        except socket.timeout:
                            try:
                                backend.shutdown(socket.SHUT_RDWR)
                            except:
                                print("exception shutting down socket after timout")
                            finally:
                                backend = None
                                theboys.pop(i)
                        except:
                            try:
                                backend.shutdown(socket.SHUT_RDWR)
                            except:
                                print("exception shutting down")
                            finally:
                                backend = None
                                theboys.pop(i)
                        else:
                            break
                    if backend:
                        msg = b""
                        packet = DataPacket(backend)
                        nchunks = packet.get_len()
                        for x in range(0, nchunks):
                            packet.get_len()
                            packet.get_chunk()
                            # send length
                            conn.send(packet.encode_length())
                            # send chunk
                            conn.send(packet.chunk)
                    conn.close()
        except KeyboardInterrupt:
            [be.shutdown(socket.SHUT_RDWR) for be in theboys]
            self.sock.shutdown(socket.SHUT_RDWR)
            print("Peace.")

if __name__ == "__main__":
    Server(3000).acceptance()

