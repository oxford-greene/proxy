import socket
import traceback

LEN_MAX = 5 # bytes

class Error:
    BAD_LENGTH = -1
    CONN_CLOSED = -2

class DataPacket:
    def __init__(self, sock):
        self.len = 0
        self.chunk = b""
        self.sock = sock

    def get_len(self):
        ## Len is max 5 bytes: ex: 65536
        oldtimeout = self.sock.gettimeout()
        self.sock.settimeout(40)
        num = b""
        try:
            while len(num) < LEN_MAX:
                data = self.sock.recv(LEN_MAX - len(num))
                if len(data) == 0:
                    return Error.CONN_CLOSED
                else:
                    num += data
            self.len = int(num)
            return self.len
        except:
            print("bad length: {}".format(num))
            return Error.BAD_LENGTH
        finally:
            self.sock.settimeout(oldtimeout)

    def get_chunk(self):
        if self.len <= 0:
            print("invalid length for chunk: {}".format(self.len))
            return
        oldtimeout = self.sock.gettimeout()
        try:
            self.sock.settimeout(40)
            while len(self.chunk) < self.len:
                data = self.sock.recv(self.len - len(self.chunk))
                if len(data) == 0:
                    return Error.CONN_CLOSED
                else:
                    self.num += data
        finally:
            self.sock.settimout(oldtimeout)

    def encode_length(self):
        return "{:05d}".format(self.len).encode("ascii")

